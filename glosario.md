# Glosario		
###### Cristian Monrroy
1. **Control de versiones (VC)**
	: Es una combinación de tecnologías y practicas para seguir y controlar los cambios realizados en los ficheros del proyecto, en especifico el código fuente, en la documentación y en las páginas web, de modo que en caso de algún altercado, nueva resolución, etc, se pueda recuperar versiones específicas mas adelante. Es por eso que el control de versiones es universal, ya que este ayuda virtualmente en todos los aspectos a dirigir un proyecto, donde el nucleo de este sistema viene siendo la *gestión de cambios*, el poder identificar cambios del proyecto,  y así disponer de esta información a quien sea y como sea. ***Es un mecanismo de comunicación donde el cambio es la unidad básica de información.*** [Fuente](https://producingoss.com/es/vc.html "Fuente")
	
2. **Control de versiones distribuido (DVC)**
	: Con un formato de *versiones distribuidos* nos referimmos que, en sí, no hay un servidor que mantenga una copia del ***repositorio***, sino que está mantenida entre los clientes que usan este repositorio. En otras palabras, cada participe dentro del trabajo, tiene la posibilidad de realizar cambios de este sin interferir en el código central, y así mismo, toda modificación efectuada, podrá ser reflejada por cualquier usuario que posea el repositorio. [Fuente 1](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Fuente 1") y [Fuente 2](https://git-scm.com/book/es/v1/Empezando-Acerca-del-control-de-versiones "Fuente 2")
	
3. **Repositorio remoto y Repositorio local**
	: Los repositorios remotos son versiones de un proyecto alojados en Internet, o algún punto de la red, con los cuales, una persona extorna pueda colaborar dentro del proyecto. El repositorio local, en pocas palabras, es el anexo encontrado dentro de nuestro ordenador, donde cualquier cambio realizado será guardado en su primera instancia en el repositorio local, para luego ser enviado al remoto. [Fuente 1](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Trabajar-con-Remotos "Fuente 1") y [Fuente 2](https://colaboratorio.net/atareao/developer/2017/git-y-github-trabajando-con-repositorios-locales "Fuente 2")

4. **Copia de trabajo / Working Copy**
	: Es un duplicado del codigo, que ha sido verificado para estar relacionado al entorno de trabajo dentro del *repositorio principal*. Dentro de esta copia cualquier cambio a la versión se vera reflejada dentro de los contenidos del proyecto. [Fuente](https://stackoverflow.com/questions/581220/what-is-a-working-copy-and-what-does-switching-do-for-me-in-tortoise-svn  "Fuente")
	
5. **Área de Preparación / Staging Area**
	: Es un fichero el cual generalmente se encuentra dentro de nuestro directorio ***Git*** donde tiene contenido toda la información que se enviará dentro de tu siguiente *commit*. [Fuente](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics "Fuente")
	
6. **Preparar Cambios / Stage Changes**
	: Es en cierto sentido una *previsualización* de los cambios a realizar dentro del codigo, ya que si bien "están" estos no estan contenidos directamente dentro del repositorio.  [Fuente](https://githowto.com/staging_changes "Fuente")
	
7. **Confirmar cambios / Commit changes**
	: En pocas pocas palabras, una vez realizados las etapas anteriores a esta, y se verifiquen la información, se realiza un *commit*, para que todos los cambios efectuados en la copia, se agreguen al repositorio. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Guardando-cambios-en-el-repositorio "Fuente")
	
8. **Commit**
	: Puede ser utilizado como un sinonimo de '***cambio***' dentro del contexto, al realizar cambios dentro del codigo, se envia un *commit* para ser agregados dentro del repositorio, para ser actualizado a la ultima versión. [Fuente](https://producingoss.com/es/vc.html "Fuente")
	
9. **clone**
	: En especifico al hablar de clonar dentro del contexto, crea una copia del repositorio, dentro de nuestro repositorio local, para así ser modificado sin afectar al proyecto principal. [Fuente](https://git-scm.com/docs/git-clone "Fuente")
	
10. **pull**
	: Es utilizado para recuperar información del repositorio, ya sea después de haber enviado datos servidor para así, automáticamente unir el código con el que se está trabajando. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Fuente")
	
11. **push**
	: Cuando llegas a un punto en el que ya quieres compartir los cambios efectuados dentro del proyecto, se realiza un *push* para enviar estos datos hacia el servidor origen. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Fuente")
	
12. **fetch**
	: Lo usamos para poder recuperar toda la información que aun no es contenido en nuestro repositorio, o en otras palabras, para actualizar nuestro codigo para cada cambio que se haga al proyecto principal. [Fuente](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos "Fuente")
	
13. **merge**
	: Agrega cambios de los commits en la branch actual. Basicamente une dos o más commits. [Fuente](https://git-scm.com/docs/git-merge "Fuente")
	
14. **status**
	: Muestra el estado actual del arbol de trabajo. Muestra las rutas distintas entre el *index file* y el *HEAD* actual, las rutas que tienen diferencais entre el arbol de trabajado y el index file no son administradas y rastreadas por git. [Fuente](https://git-scm.com/docs/git-status "Fuente")
	
15. **log**
	: Muestra el registro de commits y la clave que pertenece a cada commit. [Fuente](https://git-scm.com/docs/git-log "Fuente")
	
16. **checkout**
	: Cambia entre las branches disponibles y/o restaura los ficheros de un arbol de trabajo. [Fuente](https://git-scm.com/docs/git-checkout "Fuente")
	
17. **Rama / Branch**
	: Muestra, crea y/o elimina branches. Basicamente una branch es una linea de desarrollo y se utiliza con el fin de no dañar y/o modificar el branch original. [Fuente](https://git-scm.com/docs/git-branch "Fuente")
	
18. **Etiqueta / Tag**
	: Crea, muestra, elimina o verifica la etiqueta de un objeto. Util para cuando se quiera separar versiones estables en el desarrollo. [Fuente](https://git-scm.com/docs/git-tag "Fuente")